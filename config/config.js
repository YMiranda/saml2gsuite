module.exports = {
  development: {
    app: {
      name: 'Passport SAML strategy example',
      port: process.env.PORT || 3000
    },
    passport: {
      strategy: 'saml',
      saml: {
        path: process.env.SAML_PATH || '/login/callback',
        entryPoint: process.env.SAML_ENTRY_POINT || 'https://accounts.google.com/o/saml2/idp?idpid=C032zmvqt',
        issuer: 'https://api.yairmiranda.dev',
        cert: process.env.SAML_CERT || 'MIIDdDCCAlygAwIBAgIGAWk2p98IMA0GCSqGSIb3DQEBCwUAMHsxFDASBgNVBAoTC0dvb2dsZSBJ\n' +
            'bmMuMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MQ8wDQYDVQQDEwZHb29nbGUxGDAWBgNVBAsTD0dv\n' +
            'b2dsZSBGb3IgV29yazELMAkGA1UEBhMCVVMxEzARBgNVBAgTCkNhbGlmb3JuaWEwHhcNMTkwMzAx\n' +
            'MDAyOTI0WhcNMjQwMjI4MDAyOTI0WjB7MRQwEgYDVQQKEwtHb29nbGUgSW5jLjEWMBQGA1UEBxMN\n' +
            'TW91bnRhaW4gVmlldzEPMA0GA1UEAxMGR29vZ2xlMRgwFgYDVQQLEw9Hb29nbGUgRm9yIFdvcmsx\n' +
            'CzAJBgNVBAYTAlVTMRMwEQYDVQQIEwpDYWxpZm9ybmlhMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8A\n' +
            'MIIBCgKCAQEA0CcI/Wogl1ID5xsvfJ9BUWPyvhYc0JOg3JzPtoYBBWp2niFm9kuDRaQUJLUf2yY8\n' +
            'IiNnSySizXcXA6tAWJtWagI43CtKqwrOQmVlhioUWRIix+HDKdi18xiG9X7Fv6XvHzUXEMYeda/d\n' +
            'Mf58wL1DjPB+1ZLI9tvCEXGFGLWtaYHKMsI26+Uwad1BCSpm0wzIbtpf9bFSgTPgGJ68WyYb7fxt\n' +
            'WxDFwfdRMVjY5aO7VYSG6hV3QOhBjsvN5KUMXmSXtUjX+go+lTsyT+loc/c6bHb64L/TM2KzIcYP\n' +
            'BAGepyGCm1YOu3MO7Z/SjkJUVOwweNwazuKioA0e5zxbf9up5wIDAQABMA0GCSqGSIb3DQEBCwUA\n' +
            'A4IBAQBZxhaMj1FC9WKOWjs9cXR6XUQVppLE2TvsSz8Sk3qApJIJt76/v9nEEtdaLHhicE+h8q79\n' +
            'RcEmAhkW3e69bIIAv7k2sVmoAv6r3VlQyL133tifWinAqVO7gbgh5GTi5uN5QtIjZSqaL3e+dErW\n' +
            'MeZNQEPvCCXXA2VzjgZjTA9prWR5PGlPp3z5BzmRggkSEXF7JEiGYDOlka3STzx7DCULFFSrHJYb\n' +
            'ksB6V583kNEpvavrHgNe0mQGIaofaBNVK1mbqkmwotKi52r5THDkw5So1dDTV4CgIs65qpLThYi6\n' +
            '2UNI/zaJX3/xC8Am3jmyHPGdzFhEnDiUyc1iOuLSV4zh'
      }
    }
  }
};
